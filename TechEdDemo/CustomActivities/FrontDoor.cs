﻿using System.Activities;
using System.ComponentModel;

namespace TechEdDemo
{

    [Designer(typeof(FrontDoorDesigner))]
    public sealed class FrontDoor : CodeActivity
    {
        public InArgument<bool> Open { get; set; }
        [RequiredArgument]
        public InOutArgument<OpeningObject> Door { get; set; }
        
        protected override void Execute(CodeActivityContext context)
        {
            var door = Door.Get(context);
            door.Open = Open.Get(context);
            Door.Set(context, door);
        }
    }
}
