﻿using System;
using System.Activities;
using System.ComponentModel;

namespace TechEdDemo
{
    [Designer(typeof(OpenToiletDesigner))]
    public sealed class OpenToilet : CodeActivity
    {
        [RequiredArgument]
        public InArgument<Location> Stand { get; set; }
        [RequiredArgument]
        public InOutArgument<Cat> Cat { get; set; }
        [RequiredArgument]
        public InOutArgument<OpeningObject> Toilet { get; set; }
        [RequiredArgument]
        public InArgument<OpeningObject> Door { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            var toilet = Toilet.Get(context);
            toilet.Open = true;
            Toilet.Set(context, toilet);

            var door = Door.Get(context);
            var stand = Stand.Get(context);
            var cat = Cat.Get(context);

            switch (stand)
            {
                case Location.NextToToilet:
                    {
                        Console.WriteLine("You lose part of your face");
                        break;
                    }
                case Location.Outside:
                case Location.InNextRoom:
                case Location.Curtains:
                case Location.SleepingInTheSun:
                    throw new Exception("Can't reach toilet");
                case Location.OnLid:
                    {
                        Console.WriteLine("You fall off toilet & cat kills you.");
                        break;
                    }
                case Location.InToilet:
                    {
                        Console.WriteLine("Cat drowns you trying to stay dry.");
                        break;
                    }
            }

            if (!door.Open)
            {
                cat.Location = Location.Curtains;
            }
            else
            {
                cat.Location = Location.Outside;
            }

            Cat.Set(context, cat);
        }
    }
}
