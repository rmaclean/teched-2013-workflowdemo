﻿
namespace TechEdDemo
{
    public class Cat
    {
        public Cat(string name)
        {
            this.Name = name;
            this.Location = TechEdDemo.Location.SleepingInTheSun;
            this.Dirty = true;
        }

        public string Name { get; set; }
        public bool Dirty { get; set; }
        public bool Wet { get; set; }
        public bool Soapy { get; set; }
        public bool WillKillYouLater { get; set; }
        public Location Location { get; set; }

        public override string ToString()
        {
            return string.Format("Status of {0}:\r\n"+
"\tDirty: {1}\r\n"+
"\tWet: {2}\r\n"+
"\tSoapy: {3}\r\n"+
"\tPlotting to kill you: {4}\r\n"+
"\tLocation: {5}", Name, Dirty, Wet, Soapy, WillKillYouLater, Location);
        }
    }
}
