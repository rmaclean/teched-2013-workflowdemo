﻿
namespace TechEdDemo
{
    public enum Location
    {
        AsFarBack,
        NextToToilet,
        InNextRoom,
        OnLid,
        InToilet,
        Outside,
        Curtains,
        SleepingInTheSun
    }
}
