﻿using System;
using System.Activities;
using System.ComponentModel;

namespace TechEdDemo
{
    [Designer(typeof(PlaceCatInToiletDesigner))]
    public sealed class PlaceCatInToilet : CodeActivity
    {
        [RequiredArgument]
        public InArgument<bool> CloseLid { get; set; }
        [RequiredArgument]
        public InArgument<Location> Stand { get; set; }
        [RequiredArgument]
        public InOutArgument<Cat> Cat { get; set; }
        [RequiredArgument]
        public InOutArgument<OpeningObject> Toilet { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            var cat = Cat.Get(context);
            var closeLid = CloseLid.Get(context);
            var stand = Stand.Get(context);
            var toilet = Toilet.Get(context);
            cat.WillKillYouLater = true;

            if (!toilet.Open)
            {
                Console.WriteLine("Cat escapes!");
            }
            else
            {

                if (closeLid && stand == Location.OnLid)
                {
                    cat.Wet = true;
                    cat.Soapy = true;
                    cat.Location = Location.InToilet;
                }
                else
                {
                    Console.WriteLine("Cat escapes!");
                }

                if (closeLid)
                {
                    toilet.Open = false;
                }
            }

            Cat.Set(context, cat);
            Toilet.Set(context, toilet);
        }
    }
}
