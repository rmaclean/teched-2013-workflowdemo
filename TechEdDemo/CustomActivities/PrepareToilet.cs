﻿using System;
using System.Activities;
using System.ComponentModel;

namespace TechEdDemo
{

    [Designer(typeof(PrepareToiletDesigner))]
    public sealed class PrepareToilet : CodeActivity
    {
       
        [RequiredArgument]
        public InArgument<bool> Open { get; set; }
        [RequiredArgument]
        public InArgument<float> ShampooAmount { get; set; }
        [RequiredArgument]
        public InOutArgument<OpeningObject> Toilet { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            var open = Open.Get(context);
            var shampooAmount = ShampooAmount.Get(context);
            var toilet = Toilet.Get(context);

            if (shampooAmount == 0)
            {
                throw new Exception("No soap!");
            }

            toilet.Open = open;

            Toilet.Set(context, toilet);
        }
    }
}
