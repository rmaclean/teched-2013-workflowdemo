﻿using System;
using System.Activities;
using System.ComponentModel;

namespace TechEdDemo
{

    [Designer(typeof(FlushToiletDesigner))]
    public sealed class FlushToilet : CodeActivity
    {
        [RequiredArgument]
        public InOutArgument<Cat> Cat { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var cat = Cat.Get(context);

            if (cat.Location == Location.InToilet)
            {
                cat.Soapy = false;
                cat.Dirty = false;
            }
            else
            {
                Console.WriteLine("Flushing just wastes water");
            }

            Cat.Set(context, cat);
        }
    }
}
