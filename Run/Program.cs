﻿using System;
using System.Activities;
using System.Collections.Generic;
using TechEdDemo;

namespace Run
{

    class Program
    {
        static void Main(string[] args)
        {
            var washACatWorkflow = new WashACat();
            var cat = new Cat("Fluffy");

            var input = new Dictionary<string, object>(){
                {"Kitty",cat }
            };
            
            var output = WorkflowInvoker.Invoke(washACatWorkflow, input);
            
            Console.WriteLine(output["Kitty"]);
        }
    }
}
